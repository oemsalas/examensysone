package sysone.JavaBackEnd;

import org.springframework.boot.SpringApplication;

import sysone.JavaBackEnd.api.ServiceApi;

public class Main {


	/**
	 * Main Principal 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(ServiceApi.class, args);
	}


}
