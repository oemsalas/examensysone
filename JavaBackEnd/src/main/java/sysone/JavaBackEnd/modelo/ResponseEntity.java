package sysone.JavaBackEnd.modelo;

/**
 * @author enigma
 */
public class ResponseEntity {
	private String compressed;	

	public ResponseEntity(String compress) {
		this.compressed=compress;		
	}

	public String getCompressed() {
		return compressed;
	}

	public void setCompressed(String compressed) {
		this.compressed = compressed;
	}
	
}
