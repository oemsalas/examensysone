package sysone.JavaBackEnd.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Service {
	
	/**
	 * Comprime cadena de caracteres
	 * @param texto
	 * @return String
	 */
	public static String compress(String texto) {
	      String compressed = "";
	      Pattern pattern = Pattern.compile("([\\w])\\1*");
	      Matcher matcher = pattern.matcher(texto);
	      while(matcher.find()) {
	         String group = matcher.group();
	         if (group.length() > 1) compressed += group.length() + "";
	         compressed += group.charAt(0);
	      }
	      return compressed;
	}
	
	public static void main(String args[]) {
		System.out.println(Service.compress("AAABBAAAAABBBCCCCCCCCAAAAA"));
		System.out.println(Service.compress("AAaaCcCC".toUpperCase()));
	}
}
