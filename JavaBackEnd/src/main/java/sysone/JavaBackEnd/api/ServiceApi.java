package sysone.JavaBackEnd.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import sysone.JavaBackEnd.modelo.ModeloJson;
import sysone.JavaBackEnd.modelo.ResponseEntity;
import sysone.JavaBackEnd.service.Service;

/**
 * 
 * @author enigma
 *
 */
@RestController
@EnableAutoConfiguration
public class ServiceApi {
		
	@GetMapping("/candidate")
	public String endpointsOne(HttpServletRequest request) {
		String candidato="Omar Emilio Miguel Salas";
        System.out.println(" remoteAddres: " + request.getRemoteAddr() + " /candidate : " + candidato);
        return candidato;
	}
	
	@PostMapping(value="/compress") 
	public ResponseEntity endpointsTwo(@RequestBody ModeloJson cadena, HttpServletRequest request) {
        System.out.println(" remoteHost: "+request.getRemoteHost() + " /compress ");
        if(cadena != null && cadena.getValue() != null)
        	return new ResponseEntity(Service.compress(cadena.getValue().toUpperCase()));
        else
        	return new ResponseEntity("error : falta de parametros!");
	}
}
