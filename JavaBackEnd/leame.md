### Proyecto para el desarrollo de una API REST 

#### el primer endpoint
	Programar una función que resuelva lo pedido. 
	Realizar una API REST y hostearla en un servidor público (AWS, Google Cloud, etc), exponiendo los siguientes endpoints:
	"/candidate/": Método GET que devuelve el nombre de la persona realizando este examen.

#### segundo endpoint
	"/compress/": Método POST que ejecute el servicio compress. Recibe un JSON con la siguiente estructura:
	{
	“value”: “AAABBAAAAABB”
	}
Debe retornar HTTP-200 OK y como response body:
	{
	"compressed": "3A2B5A2B"
	}
	
##### Extras:
Considerar que la cadena de texto puede contener minúsculas, pero deben sumarse
junto con las mayúsculas, tal que: "AAaaCcCC" = "4A4C"